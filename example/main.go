package main

import (
	"gitlab.com/theuberlab/thoth/log"
	"gitlab.com/theuberlab/thoth/log/example/pkg/mixed"
	"gitlab.com/theuberlab/thoth/log/example/pkg/noisy"
)

func main() {
	log.Errorf("This logs at the %s level", "error")
	log.Warnln("This logs at the warn level")
	log.Info("This logs at the info level")
	log.DebugMap("This is a debug map", "key1", "value one")
	log.Trace("This logs at the trace level")

	mixed.MakeQuietLogs()
	mixed.MakeNoisyLogs()
	noisy.MakeLogs()
}

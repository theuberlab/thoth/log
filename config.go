package log

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/ghodss/yaml"
	"github.com/rs/zerolog/log"
	"gitlab.com/theuberlab/thoth/log/level"
)

var (
	config       *LogConfig
	splunkWriter *SplunkWriter
)

//TODO: Turn files into a map with filename as the key.
type LogConfig struct {
	Default struct {
		LevelString   string `json:"level"`
		Level         level.LogLevel
		DefaultLabels []string `json:"defaultlabels"`
	} `json:"default"`
	SplunkConfig struct {
		Token      string `json:"token"`
		Url        string `json:"url"`
		Source     string `json:"source"`
		Sourcetype string `json:"sourcetype"`
		Index      string `json:"index"`
	} `json:"splunk_config"`
	Files []struct {
		LevelString string `json:"level"`
		Level       level.LogLevel
		Filename    string `json:"name"`
	} `json:"files"`
}

func getVarStripped(varname string) string {
	if strings.Contains(varname, "$") {
		varname = strings.ReplaceAll(varname, "$", "")
	}
	return os.Getenv(varname)
}

func initConfigFromFile(filename string) *LogConfig {
	var config LogConfig

	content, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Error().Caller().Str("filename", filename).Err(err).Msg("Error reading file")
		return initDefaultConfig()
	}

	err = yaml.Unmarshal(content, &config)
	if err != nil {
		log.Error().Caller().Err(err).Msg("Error unmarshalling yaml to json")
		return initDefaultConfig()
	}

	config.Default.Level = level.LogLevelFromString(config.Default.LevelString)

	if config.SplunkConfig.Token != "" {
		splunkWriter = initSplunkWriter(
			getVarStripped(config.SplunkConfig.Token),
			getVarStripped(config.SplunkConfig.Url),
			getVarStripped(config.SplunkConfig.Source),
			getVarStripped(config.SplunkConfig.Sourcetype),
			getVarStripped(config.SplunkConfig.Index),
		)
	}

	for idx, item := range config.Files {
		determinedLevel := level.LogLevelFromString(item.LevelString)
		config.Files[idx].Level = determinedLevel
	}

	return &config
}

func initDefaultConfig() *LogConfig {
	config := LogConfig{
		Default: struct {
			LevelString   string `json:"level"`
			Level         level.LogLevel
			DefaultLabels []string `json:"defaultlabels"`
		}{
			LevelString:   "ERROR",
			Level:         level.ERROR,
			DefaultLabels: nil,
		},
		Files: nil,
	}

	return &config
}

func init() {
	configFile := "NOT FOUND"

	cwd, err := os.Getwd()
	if err != nil {
		log.Error().Caller().Err(err).Msg("Error determining working directory")
		cwd = "/"
	}

	homeDir, err := os.Getwd()
	if err != nil {
		log.Error().Caller().Err(err).Msg("Error determining home directory")
		homeDir = "/"
	}

	fileLocations := []string{
		filepath.Join(cwd, "logconfig.yml"),
		filepath.Join(cwd, "logconfig.yaml"),
		filepath.Join(homeDir, "logconfig.yml"),
		filepath.Join(homeDir, "logconfig.yaml"),
		"/etc/thoth/logconfig.yml",
	}

	for _, location := range fileLocations {
		if _, err := os.Stat(location); !os.IsNotExist(err) {
			configFile = location
		}
	}

	if configFile == "NOT FOUND" {
		config = initDefaultConfig()
	} else {
		config = initConfigFromFile(configFile)
	}

}

func initLogger(config *LogConfig) {

}

# log

## About
Thoth's log package provides structured logging output and log4j style
per-"class" (per filename in the original code) log configuration which
can be supplied via a configuration file at run time. It is a complete
rewrite of https://gitlab.com/theuberlab/lug/


## Usage
At it's simplest you can just include Thoth Log and write to it like you would with your favorite
logging package.

Without any configuration file Thoth Log configures a logger to log at
level ERROR and above.

This code:
```go
package main

import (
	"gitlab.com/theuberlab/thoth/log"
)

func main() {
	log.Errorf("This logs at the %s level", "error")
	log.Warnln("This logs at the warn level")
	log.Info("This logs at the info level")
	log.DebugMap("This is a debug map", "key1", "value one")
	log.Trace("This logs at the trace level")
}
```

Produces the output:
```
{"level":"error","caller":"/Users/x4e5/go/src/gitlab.com/theuberlab/thoth/log/example/main.go","line":"26","time":"2021-04-04T12:01:48-07:00","message":"This logs at the error level"}
```

The same code with the following config:
```yaml
default:
  level: "debug"
```

Produces the output:
```json
{"level":"error","caller":"/Users/x4e5/go/src/gitlab.com/theuberlab/thoth/log/example/main.go","line":"26","time":"2021-04-04T12:04:12-07:00","message":"This logs at the error level"}
{"level":"warn","caller":"/Users/x4e5/go/src/gitlab.com/theuberlab/thoth/log/example/main.go","line":"27","time":"2021-04-04T12:04:12-07:00","message":"This logs at the warn level\n"}
{"level":"info","caller":"/Users/x4e5/go/src/gitlab.com/theuberlab/thoth/log/example/main.go","line":"28","time":"2021-04-04T12:04:12-07:00","message":"This logs at the info level"}
{"level":"debug","key1":"value one","caller":"/Users/x4e5/go/src/gitlab.com/theuberlab/thoth/log/example/main.go","line":"29","time":"2021-04-04T12:04:12-07:00","message":"This is a debug map"}
```

[See the example directory](example/) for a more full fledged example


##  config file location search order
Thoth Log looks for a configuration file in the following locations and order.

```go
filepath.Join(cwd, "logconfig.yml"),
filepath.Join(cwd, "logconfig.yaml"),
filepath.Join(homeDir, "logconfig.yml"),
filepath.Join(homeDir, "logconfig.yaml"),
"/etc/thoth/logconfig.yml",
```

## Generatl Configuration
Thoth/log supports configuring a default log level as well as a separate
level for individual files in your project. Log level values are not
case sensitive.

### Global Log Level
Configure the global log level in the `default` section:
```yaml
default:
  level: "<your desired global logging level>"
```

### Log Levels for Specific files.
To set different log levels use the `files` config block.
This takes an array of `name` `level` pairs.

```yaml
files:
  - name: "pkg/mixed/noisyFile.go"
    level: "debug"
  - name: "pkg/mixed/quietFile.go"
    level: "Info"
  - name: "pkg/noisy/noisyPackage.go"
    level: "trAcE"
```

### Splunk Hec Logging
Thoth/log also supports logging directly to splunk cloud.
The `splunk_config` section takes five required keys.
For the value use the name of an environment variable. Thoth/log
will read the actual values from the environment variable.
Do not write these values statically in the config file.

```yaml
splunk_config:
  token: "SPLUNKCLOUD_TOKEN"
  url: "SPLUNKCLOUD_URL"
  source: "SPLUNKCLOUD_SOURCE"
  sourcetype: "SPLUNKCLOUD_SOURCETYPE"
  index: "SPLUNKCLOUD_INDEX"
```

## Config file example
A complete config file might look like this.

```yaml
# Example config file
default:
  level: "ERROR"
files:
  - name: "pkg/mixed/noisyFile.go"
    level: "debug"
  - name: "pkg/mixed/quietFile.go"
    level: "Info"
  - name: "pkg/noisy/noisyPackage.go"
    level: "trAcE"
# Configure thoth/log to log to splunk by setting the following environment variables.
splunk_config:
  token: "SPLUNKCLOUD_TOKEN"
  url: "SPLUNKCLOUD_URL"
  source: "SPLUNKCLOUD_SOURCE"
  sourcetype: "SPLUNKCLOUD_SOURCETYPE"
  index: "SPLUNKCLOUD_INDEX"
```

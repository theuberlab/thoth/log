module gitlab.com/theuberlab/thoth/log

go 1.16

require (
	github.com/ghodss/yaml v1.0.0
	github.com/rs/zerolog v1.21.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

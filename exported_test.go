package log

import (
	"github.com/rs/zerolog"
	"gitlab.com/theuberlab/thoth/log/level"
	"os"
	"testing"
)

func TestLogger_Info(t *testing.T) {
	type fields struct {
		Level  level.LogLevel
		logger zerolog.Logger
	}
	type args struct {
		message string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Log a message",
			fields: fields{
				Level:  level.INFO,
				logger: zerolog.New(os.Stderr).With().Caller().Logger(),
			},
			args: args{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &Logger{
				Level:  tt.fields.Level,
				logger: tt.fields.logger,
			}
			l.Info("This is my message.")
		})
	}
}

//func TestLogger_InfoMap(t *testing.T) {
//	type fields struct {
//		Level  level.LogLevel
//		logger zerolog.Logger
//	}
//	type args struct {
//		message    string
//		addlFields []interface{}
//	}
//	tests := []struct {
//		name   string
//		fields fields
//		args   args
//	}{
//		// TODO: Add example cases.
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			l := &Logger{
//				Level:  tt.fields.Level,
//				logger: tt.fields.logger,
//			}
//		})
//	}
//}
//
//func TestLogger_Infof(t *testing.T) {
//	type fields struct {
//		Level  level.LogLevel
//		logger zerolog.Logger
//	}
//	type args struct {
//		format string
//		args   []interface{}
//	}
//	tests := []struct {
//		name   string
//		fields fields
//		args   args
//	}{
//		// TODO: Add example cases.
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			l := &Logger{
//				Level:  tt.fields.Level,
//				logger: tt.fields.logger,
//			}
//		})
//	}
//}
//
//func TestLogger_Infoln(t *testing.T) {
//	type fields struct {
//		Level  level.LogLevel
//		logger zerolog.Logger
//	}
//	type args struct {
//		message string
//	}
//	tests := []struct {
//		name   string
//		fields fields
//		args   args
//	}{
//		// TODO: Add example cases.
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			l := &Logger{
//				Level:  tt.fields.Level,
//				logger: tt.fields.logger,
//			}
//		})
//	}
//}
//
//func TestNewLogger(t *testing.T) {
//	type args struct {
//		logLevel level.LogLevel
//	}
//	tests := []struct {
//		name string
//		args args
//		want *Logger
//	}{
//		// TODO: Add example cases.
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			if got := NewLogger(tt.args.logLevel); !reflect.DeepEqual(got, tt.want) {
//				t.Errorf("NewLogger() = %v, want %v", got, tt.want)
//			}
//		})
//	}
//}
//
//func Test_getCaller(t *testing.T) {
//	tests := []struct {
//		name string
//		want string
//	}{
//		// TODO: Add example cases.
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			if got := getCaller(); got != tt.want {
//				t.Errorf("getCaller() = %v, want %v", got, tt.want)
//			}
//		})
//	}
//}

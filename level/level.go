package level

import "strings"

type LogLevel int

const (
	NONE LogLevel = iota
	FATAL
	ERROR
	WARN
	INFO
	DEBUG
	TRACE
)

// String returns a string version of the LogLevel.
func (l *LogLevel) String() string {
	names := [...]string{
		"NONE",
		"FATAL",
		"ERROR",
		"WARN",
		"INFO",
		"DEBUG",
		"TRACE",
	}

	return names[*l]
}

// LogLevelFromString returns a LogLevel for the given string.
func LogLevelFromString(level string) LogLevel {
	enums := make(map[string]LogLevel)

	enums["NONE"] = NONE
	enums["FATAL"] = FATAL
	enums["ERROR"] = ERROR
	enums["WARN"] = WARN
	enums["INFO"] = INFO
	enums["DEBUG"] = DEBUG
	enums["TRACE"] = TRACE

	// Normalize the level string before selecting the level.
	return enums[strings.ToUpper(level)]
}

package log

import (
	"fmt"
	"os"
	"runtime"
	"strconv"
	"strings"

	"github.com/rs/zerolog"
	"gitlab.com/theuberlab/thoth/log/level"
)

type Logger struct {
	zerolog.Logger
	// loggerConfig holds the map of log levels.
	loggerConfig LogConfig
	// The level of messages which should be emitted.
	Level level.LogLevel
	// logger is the actual logger
	logger zerolog.Logger
}

var (
	logger zerolog.Logger
)

func init() {
	if splunkWriter != nil {
		multi := zerolog.MultiLevelWriter(os.Stderr, splunkWriter)
		logger = zerolog.New(multi).With().Timestamp().Logger()
	} else {
		logger = zerolog.New(os.Stderr).With().Timestamp().Logger()
	}
}

// getCaller Returns the name of the file and the line number from which the message originated
func getCaller() (string, int) {
	_, file, line, ok := runtime.Caller(2)

	//TODO: Figure out how to get the logger package to respect my log level without effing things up.

	//log.Logger = log.Level(zerolog.ErrorLevel)
	if ok {
		//log.Debug().Caller().Msg("Determined caller")
	} else {
		//log.Debug().Caller().Msg("Unable to determine caller")
	}

	return file, line
}

// Check to see if the caller is configured for the specified level
func doesSourcefileSupportLevel(caller string, logLevel level.LogLevel) bool {
	//log.Debug().Caller().Str("File", caller).Msg("Checking Level for source")
	for _, fileConfig := range config.Files {
		if strings.HasSuffix(caller, fileConfig.Filename) {
			// We do have a specific entry for this file. Now lets see if it supports the specified level.
			if fileConfig.Level >= logLevel {
				//log.Debug().Caller().Str("File", caller).Str("Desiredlevel", logLevel.String()).Str("ConfiguredLevel", fileConfig.Level.String()).Msg("Source has a level defined and it includes the level for this message.")
				return true
			} else {
				//log.Debug().Caller().Str("File", caller).Str("Desiredlevel", logLevel.String()).Str("ConfiguredLevel", fileConfig.Level.String()).Msg("Source has a level defined but it is higher than the level for this message.")
				return false
			}
		}
	}

	// If we are here we did not find a specific entry so use the default config
	if config.Default.Level >= logLevel {
		//log.Debug().Caller().Str("File", caller).Str("Desiredlevel", logLevel.String()).Str("ConfiguredLevel", config.Default.Level.String()).Msg("Default level covers this file and message.")
		return true
	} else {
		//log.Debug().Caller().Str("File", caller).Str("Desiredlevel", logLevel.String()).Str("ConfiguredLevel", config.Default.Level.String()).Msg("File has no specific config and does not match default level.")
		return false
	}
}

// ##################################################################################################################
// Fatal
// ##################################################################################################################

// Fatal logs at the info level.FATAL level.
func Fatal(message string) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.FATAL) {
		logger.Fatal().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(message)
	}
}

// Fatalln logs a line at the level.FATAL level.
func Fatalln(message string) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.FATAL) {
		logger.Fatal().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(fmt.Sprintf("%s\n", message))
	}
}

// Fatalf logs a message according to a format specified at the level.FATAL level.
func Fatalf(format string, args ...interface{}) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.FATAL) {
		logger.Fatal().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(fmt.Sprintf(format, args...))
	}
}

// FatalMap logs a message at the level.FATAL level with the provided additional fields included as key=value pairs.
func FatalMap(message string, addlFields ...interface{}) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.FATAL) {
		fields := make(map[string]interface{})

		for i := 0; i <= len(addlFields)-1; i += 2 {
			fields[addlFields[i].(string)] = addlFields[i+1]
		}

		logger.Fatal().Str("caller", caller).Str("line", strconv.Itoa(line)).Fields(fields).Msg(message)
	}
}

// ##################################################################################################################
// Error
// ##################################################################################################################

// Error logs at the info level.ERROR level.
func Error(message string) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.ERROR) {
		logger.Error().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(message)
	}
}

// Errorln logs a line at the level.ERROR level.
func Errorln(message string) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.ERROR) {
		logger.Error().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(fmt.Sprintf("%s\n", message))
	}
}

// Errorf logs a message according to a format specified at the level.ERROR level.
func Errorf(format string, args ...interface{}) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.ERROR) {
		logger.Error().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(fmt.Sprintf(format, args...))
	}
}

// ErrorMap logs a message at the level.ERROR level with the provided additional fields included as key=value pairs.
func ErrorMap(message string, addlFields ...interface{}) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.ERROR) {
		fields := make(map[string]interface{})

		for i := 0; i <= len(addlFields)-1; i += 2 {
			fields[addlFields[i].(string)] = addlFields[i+1]
		}

		logger.Error().Str("caller", caller).Str("line", strconv.Itoa(line)).Fields(fields).Msg(message)
	}
}

// ##################################################################################################################
// WARN
// ##################################################################################################################

// Warn logs at the info level.WARN level.
func Warn(message string) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.WARN) {
		logger.Warn().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(message)
	}
}

// Warnln logs a line at the level.WARN level.
func Warnln(message string) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.WARN) {
		logger.Warn().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(fmt.Sprintf("%s\n", message))
	}
}

// Warnf logs a message according to a format specified at the level.WARN level.
func Warnf(format string, args ...interface{}) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.WARN) {
		logger.Warn().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(fmt.Sprintf(format, args...))
	}
}

// WarnMap logs a message at the level.WARN level with the provided additional fields included as key=value pairs.
func WarnMap(message string, addlFields ...interface{}) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.WARN) {
		fields := make(map[string]interface{})

		for i := 0; i <= len(addlFields)-1; i += 2 {
			fields[addlFields[i].(string)] = addlFields[i+1]
		}

		logger.Warn().Fields(fields).Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(message)
	}
}

// ##################################################################################################################
// INFO
// ##################################################################################################################

// Info logs at the info level.INFO level.
func Info(message string) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.INFO) {
		logger.Info().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(message)
	}
}

// Infoln logs a line at the level.INFO level.
func Infoln(message string) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.INFO) {
		logger.Info().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(fmt.Sprintf("%s\n", message))
	}
}

// Infof logs a message according to a format specified at the level.INFO level.
func Infof(format string, args ...interface{}) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.INFO) {
		logger.Info().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(fmt.Sprintf(format, args...))
	}
}

// InfoMap logs a message at the level.INFO level with the provided additional fields included as key=value pairs.
func InfoMap(message string, addlFields ...interface{}) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.INFO) {

		fields := make(map[string]interface{})

		for i := 0; i <= len(addlFields)-1; i += 2 {
			fields[addlFields[i].(string)] = addlFields[i+1]
		}

		logger.Info().Fields(fields).Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(message)
	}
}

// ##################################################################################################################
// DEBUG
// ##################################################################################################################

// Debug logs at the info level.DEBUG level.
func Debug(message string) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.DEBUG) {
		logger.Debug().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(message)
	}
}

// Debugln logs a line at the level.DEBUG level.
func Debugln(message string) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.DEBUG) {
		logger.Debug().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(fmt.Sprintf("%s\n", message))
	}
}

// Debugf logs a message according to a format specified at the level.DEBUG level.
func Debugf(format string, args ...interface{}) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.DEBUG) {
		logger.Debug().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(fmt.Sprintf(format, args...))
	}
}

// DebugMap logs a message at the level.DEBUG level with the provided additional fields included as key=value pairs.
func DebugMap(message string, addlFields ...interface{}) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.DEBUG) {

		fields := make(map[string]interface{})

		for i := 0; i <= len(addlFields)-1; i += 2 {
			fields[addlFields[i].(string)] = addlFields[i+1]
		}

		logger.Debug().Fields(fields).Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(message)
	}
}

// ##################################################################################################################
// TRACE
// ##################################################################################################################

// Trace logs at the info level.TRACE level.
func Trace(message string) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.TRACE) {
		logger.Trace().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(message)
	}
}

// Traceln logs a line at the level.TRACE level.
func Traceln(message string) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.TRACE) {
		logger.Trace().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(fmt.Sprintf("%s\n", message))
	}
}

// Tracef logs a message according to a format specified at the level.TRACE level.
func Tracef(format string, args ...interface{}) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.TRACE) {
		logger.Trace().Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(fmt.Sprintf(format, args...))
	}
}

// TraceMap logs a message at the level.TRACE level with the provided additional fields included as key=value pairs.
func TraceMap(message string, addlFields ...interface{}) {
	caller, line := getCaller()
	if doesSourcefileSupportLevel(caller, level.TRACE) {

		fields := make(map[string]interface{})

		for i := 0; i <= len(addlFields)-1; i += 2 {
			fields[addlFields[i].(string)] = addlFields[i+1]
		}

		logger.Trace().Fields(fields).Str("caller", caller).Str("line", strconv.Itoa(line)).Msg(message)
	}
}

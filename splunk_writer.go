package log

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type SplunkWriter struct {
	Token      string
	Url        string
	Source     string
	Sourcetype string
	Index      string
}

func initSplunkWriter(token, url, source, sourcetype, index string) *SplunkWriter {
	return &SplunkWriter{
		Token:      token,
		Url:        url,
		Source:     source,
		Sourcetype: sourcetype,
		Index:      index,
	}
}

func (s *SplunkWriter) Write(data []byte) (int, error) {
	// implement writing to splunk here

	// create json object
	values := map[string]string{
		"event":      string(data),
		"source":     s.Source,
		"sourcetype": s.Sourcetype,
		"index":      s.Index,
	}
	json_data, err := json.Marshal(values)

	if err != nil {
		fmt.Println("Error Marshalling json:")
		fmt.Println(err)
	}

	// create http client
	client := http.Client{}

	// create Post request to provided URL
	request, err := http.NewRequest("POST", s.Url, bytes.NewBuffer((json_data)))

	if err != nil {
		fmt.Println("Error creating request:")
		fmt.Println(err)
	}

	// Set request headers
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", "Splunk "+s.Token)

	// make the request
	resp, err := client.Do(request)

	if err != nil {
		fmt.Println("Error making request to Splunk:")
		fmt.Println(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading resp.Body")
		fmt.Println(err)
	}

	if resp.StatusCode != 200 {
		fmt.Println("Splunk returned non 200 status code.")
		fmt.Println(string(body))
	}

	resp.Body.Close()

	return len(data), nil
}
